


Installation commands
* https://codecraft.tv/courses/angularjs-migration/step-2-typescript-and-webpack/using-webpack/
```
npm install rimraf ts-loader typescript webpack --save-dev
npm install -g tslint typescript

```
* rimraf is an executable that is used to clean the installed node packages in a node based project.
* ts-loader is a Typescript loader for Webpack.


## Extra step to do in linux
```
npm install -g yarn webpack-cli
```



running manually the webpack
```
./node_modules/.bin/webpack --bail --progress --profile 

```

## I also followed
```
https://code.visualstudio.com/docs/introvideos/configure
```