

Video 1: Introduction to Pages https://screencast-o-matic.com/watch/crVoDR9eqi



Video 2: Adding Modules and Links to a page https://screencast-o-matic.com/watch/crVobn9eY0
Video 3: Adding Pages to the Sidebar https://screencast-o-matic.com/watch/crVobf9er6
Video 4: Deeper into editing and building a module, the navigation is now much easier: https://screencast-o-matic.com/watch/crVob69e0Y
Last Video, just going over it all again and building out a bit of a system from scratch, also I had forgotten to mention a manage pages section that we have now too, you can see it about 4/5 mins into this, to be honest its not really needed as you can manage them all from each page too: 
https://screencast-o-matic.com/watch/crVobt9eU9