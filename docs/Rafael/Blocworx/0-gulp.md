# Gulp

## Installation
Gulp its a javascript package that you will need to install to be able to use
Sass or Less, in our case we are going to have a sass project, and for that you
will need to run on a terminal:
```
npm install gulp -g
```
> Note: this will install gulp globally in your computer, so you will
>have it as a terminal command.