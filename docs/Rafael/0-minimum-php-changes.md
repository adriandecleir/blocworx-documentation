# Minimum changes that you will need to do in vagrant

## How to remove Class 'DOMDocument' not found?
```
    sudo apt-get install php5.6-xml php7.0-xml php7.1-xml php7.2-xml php7.3-xml && sudo service apache2 restart
```

## How to remove Class 'ZipArchive' not found?

```
    sudo apt-get install php5.6-zip php7.0-zip php7.1-zip php7.2-zip php7.3-zip && sudo service apache2 restart
```
