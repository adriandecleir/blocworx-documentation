# Code Suggestions
This is the place that I will start to organize Ideas for our project, I will be listing as I get the Idea, so be free to comment on anything as well be free to chat about.


## Index.html
* file: application/index.html
In this file we are loading all controllers at the start of the project, maybe this can be a place to refactor the way to load javascript and css, so wont be necessary to load if isn't being used.

## app.js
* file : application/js/app.js
Line 56: apiUrl cant be found in this file, it is being imported in the index.html
```
    $authProvider.loginUrl = apiUrl + 'authenticate';
```
Line 68: as we arent loading the supportRoutes for this file, i might say that the load of this on each page will be a hard load, as i believe that in some routes this wont be necessary but the javascript class file its being used in all pages


