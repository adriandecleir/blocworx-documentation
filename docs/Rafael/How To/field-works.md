# Fields

# Station field suggestions
* After you click on the filed type, I think that should hide all items
* I could notice that we have a lot of #scanStationForm fields: read https://softwareengineering.stackexchange.com/questions/127178/two-html-elements-with-same-id-attribute-how-bad-is-it-really
* when we click to the span element isn't selecting the radio
```
<div class="new-or-edit-field-wrapper" data-children-count="1">
    <input type="radio" ng-model="scanStation.newField.field_type" value="seperator" 
           class="ng-pristine ng-untouched ng-valid ng-not-empty" name="38">
           <span>Seperator Field</span>
</div>
```
*

# Standard Text
This is a text field, that can have option content be **Uppercase** or **Mixed Case**, this will generate only the textfield, so if necessary a label

[td1]: <> (TODO: how to get a label for a textfield)

# Large Text Area
This is a textarea field, with same **Uppercase** or **Mixed Case**, logic from the standard text.

# Checkbox
This is a singular checkbox element, this will result with a checkbox plus the specified name of this field. You will also need to add the values when is checked and when isn't, the pattern is:
```
valueOn,valueOff
```

## Populate Checked Value with other local data
This is a way to create a select field that will get data from another local data.

[q1]: <> (Q: How can I generate a local data?)


# Button
This generate a label and a button field.

[q2]: <> (Q: How can I change the button collor?)

# HTML Field
This allows who is creating this component to have html tags.

[q3]: <> (Q: Do we have exceptions for this rule?)

# Seperator Field
This just create a <hr /> element

[qx]: <> (Q: Do we have a typo here? should it be Separator?)


# Digital Signature
This field contains a button, and the action of this element is to show a Digital Signature, that is loaded by project configuration signature path.

[q4]: <> (Q: Where is saved this configuration??)
[q5]: <> (Q: Do we have a table for those?)

# Date/Time Selector
You can have a way to deal with date and time into the system, basically when you select this field you will get 3 options **Datetime filed**, **Date field** and **Time field**.
* datetime field: this is an agularjs-datetime-picker, you have the option to select a date in a calendar, and some progress bars representing the hour;
* date field: this option contains a text field and a button, the action of open the uib-datepicker-popup and after you select the day, it will populate the text field with correct value.
* time field: those are text fields that contains extra functionalities, we have on each text field 2 arrows that represent + or - of a time, so the population of the filed can be done by a keyboard or mouse.

* Bug: I could setup a minus value in the time field textbox
* Bug2: Css of this field is broken at the second MM
* 

# Blocworx User Name

# Blocworx User Email

Cartolytics User Password

Cartolytics User Button

# Multi Button

[Improviment]: <> (Q: Can we remove the phrase come sparate with no spaces? we can get the data and remove all spaces, so no need to say that)

# Numeric/Quantity

# File

# Dropdown From Station

# WYSIWYG Field
# WYSIWYG Field For User
# Jobs List Dropdown
# Alerts Button
# Auto Generate Field
# Autocomplete Field
# Formception
# Total Results
# Time Difference
# Radio Buttons From Station
# Dropdown From List
# Plain Data Field
# Label Field (Values Not Tracked)
# User Select Field
# Form To Generate Field
# Section Selector (For Form Generation)
# Image URL Field
# Maths
# Results Box
# Logged in Users Name (shows users name as plain data)
# Formception 2: Electric Boogaloo
# Form List
