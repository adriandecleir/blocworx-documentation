# Show more

For that you will need to do one of this 2 actions:
* Adding a size limit to a field
* Choosing "Tick this box if you would like to hide this field from any data given back for this field." 

## Adding a size limit to a field
* go to edit mode
* hover a field -> parameters
* set some value on: "Set Character View Limit"
* you will be able to see **show more**

## Tick this box if you would like to hide this field from any data given back for this field
* go to edit mode
* hover a field -> parameters
* Select the option **Tick this box if you would like to hide this field from any data given back for this field.**
* you will be able to see **show more**
 
                     
