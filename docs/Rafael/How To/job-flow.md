# How to create a Job?
This is the flow of the Job creation, you must do the following:

* Open homepage
* In the home menu item choose **Jobs** -> **My Active Jobs**
* Now that you are on Job & Station Builder you must click on **Create Job**
* Add a Job title as its required, and a description of it as minimum to create a job
* click to save it
Those are the steps for a new Job into the system.

# How to setup things in a job?
On the **Job & Station Builder** you will get a list of jobs that you have on the project database, and on each row you will get some job functionalities, in this case we must click on **Job Setup**

## Job Setup page
In this page we can Import from a **Job template**, **Create & Delete Stations**, **Upload SOP's** and **Edit of the Job details**

### Import From Job Template
This part will guide you to import the data **from**

(TODO: you must find what are the things on this list to update here!!)

* Q: How the Import from template works, so could be nice a task that involves that? I could know more about the import action in the system, I think that should be a big thing in it.

### Upload SOP
This is the place that you can upload SOP files, you will see a table with all the files that you uploaded

* Q: After an upload those files, are they available for the whole system? or only for this specific Job?
* Q2: What SOPs stands for? could be nice someone that knows the nomenclature of things has on the documentation, just to avoid any kind of ambiguity, is it SOP stands for Standard Operating Procedure?

### Edit Job Details
Here you will be able to edit some data that you want to change related to this Job, basically it's the same form of create a new job with all the stored data being loaded in this page and available for an update of it.

### Create & Delete Stations
In this screen you will see 3 main tabs, **Add new Station**, **Add Section Station** and **Generate Form from Another Form**

#### Create a new Station
You must use the first block **Add New Station**, just pop in a name, and you will get a new station, if you won't select a Section it will be generated a Station with **no section assigned**

#### Create a new Section
You must only specify what will be the section for the station, so this is used to organize Stations

* Q: In plain words, what should be the best way of thinking what is a Section for a station? like the section Station could be a worker, and a section could be a team, is it something like that?

* Improvement: I was checking the flow, so I started with a new job, after I realized that was on a No Section Assigned section in the list, and I could create a Section to organize my Station, so after create that I tried without thinking to drag and drop the new station to the new Section, could be nice this ability into the system ? maybe?


# How to manage a Station?
You must do the following to get the screen that you will be able to manage a station, so do the following:

* Homepage -> My Active Jobs -> Find the Job and click Job Setup
* Create & Edit Stations -> Click on the Station that you want to Manage
Now you can see a preview of the station, and a bunch of field types that you can add, the most important ones are:
  * Base path: **application/views/front-end/**
  * scan-station.html
  * scan-station-fields.html
  * scan-station-form-buttons.html
  * scan-station-results.html
  * scan-station-results-calendar.html
  * scan-station-results-default.html
  * scan-station-results-vertical-rows.html

## New Station field
You must type **a field name** and select what is the type of the field that you will like to be using, as basic to start we can generate a new one doing:

* On the **Station Fields** section you add a **field name** and select **standard text**, so yo will get text field, and it's important to choose between the below option to Mixed Case, as one of the options to select.
* A place to check for fields: **application/views/front-end/scan-station-fields.html**


# Possible bugs

## Bug isnt showing Create & Delete Station?
* Get the job ID from the job list, lets say **123**
* The URL for Create & Delete Stations will follow:
```
    [domain url]/job/[job ID]/create-scan-stations
```
In our case for a localhost url of the 123 job you must get: **http://demo.blocworx.local/job/123/create-scan-stations**
