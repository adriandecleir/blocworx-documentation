# manage-users

# Flow

## Find the main State
You will need to access the Admin -> Manage Profiles page, you will find that the URI of this page is **/admin/manage-users** and searching for that you will find on:

* application/js/app.js
```javascript
.state('manage-users', {
    parent: 'app',
    url: '/admin/manage-users',
    views: {
        'parent': {
            templateUrl: 'modules/Module1UserManagement/module_1_user_management.html?nd=' + Date.now(),
            controller: 'MainController as main'
        }
    }
    , params: {
        editEntity: false,
        editMode: false,
        usesRole: 'manage-users',
        sidebarHighlighter: 'admin'
    }
})
```

## Finding the template
Now that you could find the place that was stored the app states, you will be able to see the **templateUrl**, this field contains the template path, so basically you must go to the path, that in our case:
```
    modules/Module1UserManagement/module_1_user_management.html
```

## Finding the controller
You also could notice that on the state object you can see in the views -> parent -> controller, that we have:
```
views: {
    'parent': {
        templateUrl: [some kind of value],
        controller: 'MainController as main'
    }
}
```
In this part of the states you can see that is a **controller**, and for this one its **'MainController as main'**, this means that you should look for this name **MainController** though the project you fill find:

* path: application/js/controllers/MainCtrl.js
```
angular.module('MainCtrl', ['AuthService', 'ngSanitize']).controller('MainController',
    ['$http', 'Auth', '$auth', '$location', '$window', '$state', '$rootScope', '$timeout', '$scope', '$interval',
      'Data', '$sce',
      function ($http, Auth, $auth, $location, $window, $state, $rootScope, $timeout, $scope, $interval, Data, $sce) {
      [...]
    [...]
}]);
```
You can see that to be able to search for a controller we can get the pattern:
```
.controller('MainController',
```
So you can search for 'nameOfTheController' that you will find a file that started the controller, or if you want to be specific, you can specify on the search params **.controller('nameOfTheController'**, be my guest, you can use any way that you feel confortable.

# Module1UserManagementController
I could see that in this case we have this controller, you can use the way that was explained to get this controller on:

* application/modules/Module1UserManagement/Module1UserManagementCtrl.js
