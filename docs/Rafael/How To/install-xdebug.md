# How to install Xdebug on the scotch/box machine

# Finding Vagrant project root
You must know where is localized your scotch box, mine its localized at: **/home/strubloid/webroot/blocworx/box**, 
so that will be my vagrant project root folder. In this folder you must see a structure like:
```
> [/home/strubloid/webroot/blocworx/box] (git: master) 2020-11-30 13:18:48 $ ls

-rw-------  1 strubloid  241 Nov 25 09:53 2020-11-25-09-53-36.068-VBoxSVC-24768.log
drwxrwxr-x  4 strubloid 4.0K Nov 11 10:18 .vagrant/
drwxrwxr-x 17 strubloid 4.0K Nov 23 10:57 cartolytics/
drwxrwxr-x  2 strubloid 4.0K Nov 27 12:32 public/
-rw-rw-r--  1 strubloid 3.8K Nov 11 09:48 README.md
-rw-rw-r--  1 strubloid  856 Nov 30 12:28 Vagrantfile
```
Note: in this folder you will be able to see the **Vagrantfile**, that is the place that we need to know where 
it is basically.

# Creating the bootstrap.sh
* You will need to create a file and call **bootstrap.sh**
* You will place the content of this file:
```
#!/usr/bin/env bash

# updating the apt-get
apt-get update

# installing the xdebug
apt-get install -y php-xdebug

# creating a base xdebug file
cd /etc/apache2/mods-available/
touch xdebug.ini

# population of this new file using echo command
echo "zend_extension=xdebug.so
xdebug.remote_enable = on
xdebug.remote_port = 9000
xdebug.remote_connect_back = on
xdebug.idekey = XDEBUG
xdebug.show_error_trace = 1
xdebug.remote_autostart = 0" > xdebug.ini

# Copying over all the php versions available
cp xdebug.ini /etc/php/5.6/mods-available/xdebug.ini
cp xdebug.ini /etc/php/7.0/mods-available/xdebug.ini
cp xdebug.ini /etc/php/7.1/mods-available/xdebug.ini
cp xdebug.ini /etc/php/7.2/mods-available/xdebug.ini
cp xdebug.ini /etc/php/7.3/mods-available/xdebug.ini
cp xdebug.ini /etc/php/7.4/mods-available/xdebug.ini
cp xdebug.ini /etc/php/8.0/mods-available/xdebug.ini

# restarting the apache service
service apache2 restart
```

# Changing the configuration on Vagrantfile
Now we have a new bootstrap actions to do, we also need to set up the Vagrantfile to ask to load instructions from 
this file, so to do that you must:
* open the file: **box/Vagrantfile**
* add the line: ```config.vm.provision :shell, path: "bootstrap.sh"```

## Vagrant File updated sample
```
Vagrant.configure("2") do |config|
    config.vm.box = "scotch/box"
    config.vm.network "private_network", ip: "192.168.33.10"
    config.vm.hostname = "scotchbox"
    config.vm.synced_folder ".", "/var/www", :mount_options => ["dmode=777", "fmode=666"]
    config.vm.provision :shell, path: "bootstrap.sh"
end
```
> Note: you will be able to see the last line inside of the configuration **"config.vm.provision"**, that is what you
>will need to do.

# Reload the image with provisions
Now you have set up the Vagrantfile, we need to ask to load with that configuration, you must run this command outside 
of the box.
```
vagrant reload --provision
``` 
 
# How to configure PHPStorm or Webstorm?
 
* Reference: https://medium.com/@mohsinyounas05/how-to-enable-xdebug-for-laravel-with-vagrant-and-phpstorm-df5403c9e793 


