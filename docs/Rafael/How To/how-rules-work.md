# How Rules work?

# Bunch of rules
* cannot_be_empty
* must_be_equal_to
* cannot_be_equal_to 
* must_be_between_two_values
* must_be_at_least_x_number_of_characters
* must_be_exactly_x_number_of_characters
* must_exist_elsewhere 
* must_not_exist_elsewhere
* numbers_only

## url: http://demo.blocworx.local/job/881/scan-station/5872/edit
* field 1: cannot_be_empty 
* field 2: must_be_equal_to: RAFAEL, RAFA
* field 3: cannot_be_equal_to RAFA, RA
* field 4: must_be_between_two_values 100-1000
* field 5: must_be_at_least_x_number_of_characters: min 5 
* field 6: must_be_exactly_x_number_of_characters: 4 
* field 7: must_exist_elsewhere: job: 881, station: rules data, column values = data 1
* field 8: must_not_exist_elsewhere: job: 881, station: rules data, field data 2Number of characters
* field 9: numbers_only: you must add only numbers


# Specific Value Required Elsewhere
* specific_value_required_elsewhere
## url: http://demo.blocworx.local/module/881/bloc/5874
* serial number: specific_value_required_elsewhere:
```
Bloc ID (better details coming soon): 5875
Field To Search: serial_number
Field to watch: result
Value watched field must equal: PASS

{"order": 1, "param1": "5890", "param2": 108689, "param3": 108689, "param4": "RAFAEL"}
```

# Cant be present elsewhere
* specific_value_cannot_be_present_elsewhere
## url : http://demo.blocworx.local/job/881/scan-station/5876
* serial number: specific_value_cannot_be_present_elsewhere: 
```
Bloc ID (better details coming soon): 5877
Field To Search: serial_number
Field to watch: result
Value watched field cannot equal: FAIL
Exception Field:
Exception Value:
```

* serial: cannot_exist_in_another_station
```
Cannot Exist in another Station
Bloc ID (better details coming soon): 5877
Field to watch: serial_number
Exception Field: result
Exception Value: PASS
```

# Cannot Duplicate
* cannot_be_duplicate
## url: http://demo.blocworx.local/module/881/bloc/5878
* cannot be duplicate: cannot_be_duplicate : max 1, exceptions: 33333,44444
* auto generate number: auto_generate_number (MISSING ASK ADRIAN)
* must be certain format: must_be_certain_format: variable: X, Format: RAFAELXXX 

# Rule to check 2 
* must_not_be_local_duplicate (MISSING ASK ADRIAN)
* must_be_equal_to_another_local_field (MISSING ASK ADRIAN)
## url: http://demo.blocworx.local/module/881/bloc/5879
* local duplicate
* must not be local duplicate
* must be equal to another local field

# populate local field with part of this field
* populate_local_field_with_part_of_this_field
```
Populate local field with part of this field
Variable Character: %
Sample String: ID%%%
Local Field to populate: loading_data
Change Data Results: 123=Master,000=Admin,321=Rafael
```
## URL: http://demo.blocworx.local/module/881/bloc/5880

# Populate fields
* populate_another_local_field_with_data_from_elsewhere
* must_have_matching_equivalent_data_in_another_station
``` 
Must have matching equivalent data in another form
Bloc to Check: 5882
Field Matching Combinations: look_up:name, code_to_match_employees_scan_station:code
```
## URL: http://demo.blocworx.local/module/881/bloc/5881
* must_have_matching_equivalent_data_in_another_station

## URL2: http://demo.blocworx.local/module/881/bloc/5882

* populate_another_local_field_with_total_from_elsewhere
## URL: http://demo.blocworx.local/module/881/bloc/5880/


# Frontend changes
## URL: http://demo.blocworx.local/module/881/bloc/5885
* Autoincrement: auto_generate_number (This isnt a rule, it is more like a field than a rule) 
!!!! This will be deleted !!!!

* Text field: this is just a field = OK 
* Must not be duplicate of textfield: must_not_be_local_duplicate = OK  
```
Cannot Be Local Duplicate
Local Field to Compare: textfield
```
* Auto populate: populate_local_field_with_data_from_another_local_field = DOING
``` 
Populate local field with data from another local field
Field To Search: autoincrement
Field to Populate: auto_populate
Overwrite existing data: Yes
```


## keep_data_for_next_scan (MISSING ASK ADRIAN) 
## must_be_equal_to_another_local_field (MISSING ASK ADRIAN)


## Missing, when the order value is empty isnt getting the next possible value



Create the component field rules
frontend rules 
Edit rule

























That is the sequence?
1
```
composer dump-autoload
```
2
```
php artisan migrate
```

3
```
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE table `scan_station_grid`;
TRUNCATE table `scan_station_grid_fields`;  
SET FOREIGN_KEY_CHECKS = 1;
```

4
```
php artisan db:seed --class=GridSeeder
```