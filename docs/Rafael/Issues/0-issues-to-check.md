# Previous reported issues

* When we change the name of a field it sometimes breaks other fields that are connected to it
* When we change the name of a field it sometimes breaks other rules that are connected to it
* When we copy whole jobs the migration of rules is very unstable, to the point that currently for complex jobs we have advised people to simply contact us.
* When we copy whole jobs the migration of fields is sort of unstable if they are dependant on other fields
* When we copy stations on their both 3 and 4 apply
* Sometimes when we have a new field , in theory you should be able to make that field as many times as you want, this isnt always the case, sometimes the functionality can be limited
* Sometimes when you make a new rule, in theory you should be able to make that rule as many times as you you want, and again this isnt always the case, it sometimes either breaks things
* The order of rules can sometimes have an impact on the output
* The other of fields can sometimes impact functionality (like maths fields etc)
* Editing fields is unstable, we have sometimes resorted to workarounds, like just deleting and creating a new field. This one is important , in theory when we create a new field we really shouldnt ahve to worry about how the editing and parameters of this field will be handled
* Field parameters should always work on every new field and based on the parameter a decision should be made how multiple fields will be handled (example is the task you did last week with the filtering, its able to handle one field but we never made a decision on how to handle many, this kind of thing has always been and will always be coming up)

# Adrian 
* Rules are not being validated when created meaning its possible to create broken ones
* Fields are not being validated when  created meaning its possible to create broken ones

# Peter
* The problem with duplicating entire module is that if there are parameters, rules, fields settings related between 2 forms, the same relations should be included in new module between same but new 2 forms, where the settings still contains ID's of forms from the very first module




