# Colors

## Main colors
Mycolor.space can be very useful to see how colors match with the 
main webpage color. For each main color we will get a palette.

## Main color on a dark mode: #455054
* palette: https://mycolor.space/?hex=%23455054&sub=1

## Main color on a light mode: #f0f4f5
* palette: https://mycolor.space/?hex=%23F0F4F5&sub=1


