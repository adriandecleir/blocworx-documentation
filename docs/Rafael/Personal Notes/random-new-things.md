# Random new things
I will use this space to add randomly things while I am programing, and I don't want to classify them, so it is basically a paste place that I can organize later on, if I am that busy, or I don't know yet how should be the correct nomenclature for the new knowledge.

# application/views/home.html
This file contains the main template for **Features**, **Jobs** and **Others** in the **homepage section**


## Check some pull requests
```
 app/Http/Controllers/RuleController.php                  | 33 
 app/Rule.php                                             |  9 
 application/js/controllers/job-setup/scanStationCtrl.js  | 26 
 application/js/services/job-setup/scanStationService.js  |  5 
 application/views/admin/job-setup/scan-station-edit.html | 71 
```
This was the pull request for the changes in the rule:
* https://bitbucket.org/adriandecleir/cartolytics-iqutech/pull-requests/444
