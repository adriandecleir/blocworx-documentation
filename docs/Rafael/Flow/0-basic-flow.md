# How is a basic flow for the project?
In this basic flow it's necessary to show what are the things that you should be looking when you are catching up with the system, a few files that can make your whole mindset change, so it will be introduced to:
* Index.html : main place for the application view start
* App.js: Place that you will find the angular project configuration
* MainCtrl.js: Main controller of the application
* Routes/web.php

## Index.html
File: /application/index.html
This file contains the mains import of the javascript files, so if you need to load a frontend javascript
it must be necessary do add one document.write like:
```
document.write('<script src="js/services/[service-name]/[javascript class].js?timestamp=' + timestamp + '"><\/script>');
```
### Examples
```javascript
    
    // This is a javascript example
    document.write('<script src="js/services/job-setup/supportScreenRoutes.js?timestamp=' + timestamp + '"><\/script>');

    // This is a css example
    document.write('<link rel="stylesheet" href="css/angular-date-picker.css?timestamp="' + timestamp + '">');
```

## App.js
File: application/js/app.js
In this file you will find the angular project configuration, this will contain a list of modules, and a configuration for each url on a config function. This is the place that you will find **template file** and the **`controller`**.

### Finding Controller and Html template
* You must check the file: application/js/app.js
* Copy the page url: modules/job-station-builder, and try to find the url that contains this case:
```javascript
$stateProvider
.state('job-station-builder', {
    parent: 'app',
    url: '/modules/job-station-builder',
    views: {
        'parent': {
            templateUrl: 'views/modules/job-station-builder.html?nd=' + Date.now(),
            controller: 'MainController as main'
        }, params: {
            editEntity: true,
            editMode: true
        }
    },
    params: {
        usesRole: 'job-station-builder',
        sidebarHighlighter: 'home'
    }
})
```
> Note: You will find all the configuration, url, who is the view loaded as well as params applied ot it
* Template Url: This is the file that will be loaded as html template, in this case **application/views/modules/job-station-builder.html**

### Create-support-screen case
As you can see we have this setup loaded from **supportScreenRoutes** and this is imported from the **index.html**, as you can see below:
```javascript
    // This is a way to get a pre-set route from the supportScreenRoutes    
    $stateProvider.state('create-support-screen',supportScreenRoutes['create-support-screen'])
```
So to be able to access that, you can search though your project for the word **supportScreenRoutes**, and you will find the file **application/js/services/job-setup/supportScreenRoutes.js**, that contains properer configuration, you also will be able to confirm that is been loaded on the index.html:
```javascript
        document.write('<script src="js/services/job-setup/supportScreenRoutes.js?timestamp=' + timestamp + '"><\/script>');
```


