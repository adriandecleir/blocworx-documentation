# 03-how-to-check-merge-conflicts
This process it to avoid having conflicts or make some while we are on the merging process.

## Simple flow
* First of all you will need to get the code from master or develop updated.
```
  $ git checkout master && git pull origin master

  $ git checkout develop && git pull origin develop
```

* After the update of sources branches you will have an updated environment to work with
```
  $ git flow feature start your-feature-name
```

* So now with your feature/your-feature-name will be able to add new code. So you will be
working til figure it out how to fix this issue or implement this new feature.

* While you are working, its very encouraging to each step that you can see **a step forward** you must
commit what was achieved and move on with new changes, like:

```
  git add . && git commit -m "adding new way to use routes, new main service now its created"
```

> Note: be worry about what are the phrase that you will put on your computer, why? it is very important
to know what is the stage that you are, you will be able to understand how you progress, as well as your
team members can understand how are you thinking, and they can also help you out.


## Issue with merge on Bitbucket
So you did all your job, was a good one! you could fix the issue, but some files are, quite shareable,
or they are configuration points. Those places are quite easy to find a **merge conflict**, those are identified by:

![bitbucket error](../..//Rafael/img/bitbucket-conflict.png "Bitbucket Conflict")

## How to fix a merge conflict?
Basically you must update your branch, all the possible to merge branches, do a merge, see
that you will find conflicted files, and on each file you must decide what is supposed to stay
after the merge.

### Update all your develop and master branches
```
  git checkout master && git pull origin master && git checkout develop && git pull origin develop
```
### Update of your branch data
```
  git checkout feature/your-feature-name && git pull origin feature/your-feature-name
```
### Merge your feature with develop branch
```
  git merge develop
```

#### This is a response when you get a conflict after the action of merge
![Terminal Conflict](../..//Rafael/img/terminal-conflict.png "Terminal Conflict")

### Resolving the conflicts using IDE
Now you must use your IDE to check for the git diffs, using the command **git -> resolve conflicts**
![IDE Git Resolve Conflicts](../..//Rafael/img/ide-fix-conflict.png "IDE Git Resolve Conflicts")

### Conflicted files screen
You will be able to see a new screen that will contain a list of the **conflicted files**, some quick action buttons
accept yours or accept theirs, those buttons are only for actions that you know something needs to be overwritten by you 
or by the changes on develop.
![IDE Conflicts List](../..//Rafael/img/ide-conflicts-list.png "IDE Conflicts List")

#### Checking the file
To be able to see the differences between your change, and develop one, you must double-click on the conflicted file, 
and you will be able to see this screen:

![IDE Conflicts Screen](../..//Rafael/img/ide-conflicts-screen.png "IDE Conflicts Screen")
So after you finish to check aoo your changes, you can have as option:
* Ignore everything and only accept your changes
* Ignore everything and only accept develop branch changes
* You can check each change and accept from the left on each line, and after all you will se a mixed thing
 and click apply. (**recommended**) 
 
### Finishing committing the changes
After you finish all the changes, and fixed all merge conflicts, you will see **All conflicts fixed, but you are still merging.**
To see that you must do:
```
    git status
```
![Terminal Conflict Fixed](../..//Rafael/img/ide-conflicts-fixed.png "Terminal Conflict Fixed")
Now you must update your branch with changes from the merge that you did on your local machine, your branch it's more
updated than the repository one, so you must commit the changes, and push it to the server.
```
git commit -m "merge with [develop/master] branch"
git push origin feature/your-feature-name
```
