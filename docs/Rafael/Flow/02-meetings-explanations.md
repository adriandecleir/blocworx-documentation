# Meetings
* reference: https://us02web.zoom.us/rec/share/Q5FtNO3Xe_7k8LYQK5UlomddxRIa4YVxwiy2gcC1a_K6Xp0ImUwO7rHCcBYWaX94.lcZ1CLNrEvtuE_r1

## Explanations
* reference: http://somup.com/cYXbboIGT7 or https://screencast-o-matic.com/watch/cYXbbDLjN1

## How to create a job
* Go to Home -> Jobs -> My Active Jobs
* Click **Create Job**, fill the form and click save

## How to CRUD a Job
* Search by you job name on: **Home** -> **Jobs** -> **My Active Jobs**
* Finding your job you must click on **job setup**

## How to create a scan station for a Job?
* url will look like: ```http://demo.blocworx.local/admin/job/[job-id]```

## How fields works
* reference: https://screencast-o-matic.com/watch/cYlel9N0be


## How Bartender Generator works
* Issue: https://screencast-o-matic.com/watch/cYlQbGNN3F
* Coma fix: https://screencast-o-matic.com/watch/cYlQbRNNul
* Integration : https://screencast-o-matic.com/watch/cYlQFiNNww
* Final steps: https://screencast-o-matic.com/watch/cYlQFZNNBY 