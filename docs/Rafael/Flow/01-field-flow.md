# Database field flow

# Job
First of all we need to start with a job, a job its an instance on the job table

## Job tables
* job_complieance_link
* job_parameters
* job_status
* user_to_job


# Station
This is a representation of a **Scan_Station** table value, over there will have the station id and properties plus a **job_id** as foreign key

## Station Tables
Those are tables for the scan station or related to station

* scan_station￼
* scan_station_data
* scan_station_field
* scan_station_field_parameters
* scan_station_parameters
* scan_station_rule
* scan_station_to_inventory_link
* station_file
* station_linked_with_table
* station_section
* station_user


4551

# Fields
Each field it's a representation on the table **scan_station_field**, you must combine the scan_station_id with job_id to be able to get the fields of the correct screen, nas each job can have multiple stations, and each station can have many fields.

## Query sample
```sql
    SELECT * FROM `scan_station_field` 
     WHERE job_id = '677' 
       AND scan_station_id = '4551' 
  ORDER BY `scan_station_field`.`id` DESC
```
> Note: this is a way to get **all fields** from the **job 677**, on the **section 4551**.
